Source: golly
Section: games
Priority: optional
Maintainer: NIIBE Yutaka <gniibe@fsij.org>
Build-Conflicts: libwxgtk2.6-dev
Build-Depends: debhelper-compat (= 13), libwxgtk3.0-gtk3-dev, perl, libperl-dev, zlib1g-dev, dh-lua
Standards-Version: 4.6.1
Homepage: https://golly.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/golly.git
Vcs-Browser: https://salsa.debian.org/debian/golly

Package: golly
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Game of Life simulator using hashlife algorithm
 Golly simulates Conway's Game of Life with an arbitrarily large grid
 of cells.  It can optionally use a hashlife algorithm, which allows
 it to rapidly compute generations for huge patterns, and to compute
 many generations into the future at a time.
 .
 Golly provides a graphical interface for viewing and editing cellular
 automata.  It supports copy and paste, zoom, auto-fit, multiple
 layers, and viewing different areas of a pattern simultaneously in
 different areas of a window.
 .
 Golly can load patterns from RLE, Life 1.05/1.06, dblife, and
 macrocell file formats; it can also interpret images as Life
 patterns.  Golly provides integrated help, including a copy of the
 Life Lexicon.
 .
 Golly also supports other rules for 2D cellular automata with an
 8-cell neighborhood, and supports 1D cellular automata.
